<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Siswa extends CI_Controller
{


    public function index ()
    {
        $data ['title'] = 'my profile';
        $data ['siswa'] = $this->db->get_where('siswa',array('username' => $this->session->userdata('username')));
        //var_dump($data['siswa']);

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('Siswa/siswa',$data);
        $this->load->view('templates/footer');
    }
}