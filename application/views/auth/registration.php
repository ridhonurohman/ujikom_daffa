<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
        <div class="card-body p-5">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                        </div>
                        <?= $this->session->flashdata('message'); ?>
                        <form class="user" method="post" action="<?= base_url('auth/registration'); ?>">

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder=" Full Name" value="<?= set_value('nama'); ?>">
                                <?= form_error('nama', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="username" name="username" placeholder=" username " value="<?= set_value('username'); ?>">
                                <?= form_error('username', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="nisn" name="nisn" placeholder=" Nisn " value="<?= set_value('nisn'); ?>">
                                <?= form_error('nisn', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="nis" name="nis" placeholder=" Nis " value="<?= set_value('nis'); ?>">
                                <?= form_error('nis', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="id_kelas" name="id_kelas" placeholder=" Id_kelas " value="<?= set_value('id_kelas'); ?>">
                                <?= form_error('id_kelas', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="alamat" name="alamat" placeholder=" Alamat " value="<?= set_value('alamat'); ?>">
                                <?= form_error('alamat', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="no_telp" name="no_telp" placeholder=" Telephone " value="<?= set_value('no_telp'); ?>">
                                <?= form_error('no_telp', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control form-control-user" id="id_spp" name="id_spp" placeholder=" Id_spp " value="<?= set_value('id_spp'); ?>">
                                <?= form_error('id_spp', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg">
                                    <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                                    <?= form_error('password1', '<small class="text-danger" pl-3>', '</small>'); ?> </small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Register Account
                            </button>

                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="<?= base_url(); ?>">Already have an account? Login!</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

</div>