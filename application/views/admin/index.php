                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->

                    <h1 class="h3 mb-4 text-gray-800">My Profile</h1>
                    <div class="card mb-3" style="max-width: 540px;">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="<?= base_url('assets/img/profile/') . $siswa['image'];?>" class="img-fluid rounded-start" >
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title"><?= $siswa['username'];?>
        <p class="card-text"><?= $siswa['nis'];?></p>
        <p class="card-text"><?= $siswa['nisn'];?></p>
        <p class="card-text"><small class="text-muted">Member dari <?= date('d F Y', $siswa['date_created']); ?></small></p>
      </div>
    </div>
  </div>

                    
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           