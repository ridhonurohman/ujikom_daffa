 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
    <div class="sidebar-brand-icon">
    <i class="fas fa-school"></i>

    </div>
    <div class="sidebar-brand-text mx-3">WEB Prik </div>
    
</a>

<!-- Divider -->
<hr class="sidebar-divider ">



    <!--QUERY MENU-->

    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu="SELECT `user_menu`.`id`,`menu` 
                    FROM `user_menu` JOIN `user_access_menu` 
                    ON `user_menu`.`id` = `user_access_menu`.`menu_id` 
                    WHERE `user_access_menu`.`role_id`=$role_id 
                    ORDER BY `user_access_menu`.`menu_id` ASC " ;    

    $menu = $this->db->query($queryMenu)->result_array();
    
    ?>



 <!-- Heading -->
 <?php foreach ($menu as $m ) : ?>
 <div class="sidebar-heading">
    <?= $m['menu']; ?>
</div>
<?php endforeach; ?>
<!-- Nav Item - Dashboard -->
<li class="nav-item active">
    <a class="nav-link" href="<?= base_url('dashboard'); ?>">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Interface
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-user"></i>
        <span>Siswa</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Siswa:</h6>
            <a class="collapse-item" href="buttons.html">Data Siswa</a>
            
        </div>
    </div>
</li>

<!-- Nav Item - Utilities Collapse Menu -->



<!-- Heading -->
<div class="sidebar-heading">
    User
</div>

<li class="nav-item">
    <a class="nav-link" href="charts.html">
    <i class="fas fa-fw fa-user-nurse"></i>
        <span>Petugas</span></a>
</li>



<!-- Divider -->
<hr class="sidebar-divider">

<li class="nav-item">
    <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
    <i class="fas fa-fw fa-power-off"></i>
        <span>Logout</span></a>
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Logout</a>
                </div>
            </div>
        </div>
    </div>
</li>



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

<!-- Sidebar Message -->


</ul>
<!-- End of Sidebar -->
